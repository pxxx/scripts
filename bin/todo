#!/bin/bash

declare todo_dir="${TODODIR:-$HOME/Documents/notes/todos}"
declare do_each_day="${todo_dir}/.doEachDay"
declare do_each_week="${todo_dir}/.doEachWeek"

declare known_editors=(lvim nvim vim emacs vi nano gedit mousepad)
declare editor="${EDITOR:-$(for e in "${known_editors[@]}"; do command -v "$e" && break; done || echo)}"

if [ -z  "$editor" ]; then
  echo "No editor found. Set variable EDITOR or install one of the following: ${known_editors[*]}"
  exit 1
fi

function ask_yes_no_question() {
    read -p "$1 [Y/n] " -r y
    [ -z "$y" ] || [[ $y =~ ([yY]|[Yy]es) ]]
}

function get_todo_corresponding_to_offset () {
  local file_name="weekly_todo_$(date --date="+${offset_in_days}day" "+%Y")_week_$(date --date="+${offset_in_days}day" "+%W").md"
  echo "$todo_dir/$file_name"
}

function ensure_todo_dir_exists() {
  if [ ! -d "$todo_dir" ]; then
    echo "Specified directory for todos ($todo_dir) is not directory"
    if ask_yes_no_question "Do you want to create $todo_dir?"; then
      mkdir -vp "$todo_dir"
    else
      exit 1
    fi
  fi
}

function init_todo() {
  local -i day_of_week=$(date '+%u')
  local file="$1"

  {
    for e in {0..6}; do
      o="$((offset_in_days - day_of_week + 1 + e))"
      echo -e "# $(date --date="+${o}day" "+%A") $(date --date="+${o}day" "+%F"):"
      if [ -f "$do_each_day" ]; then
        cat "$do_each_day"
      fi
      echo -e "\n"
    done

    echo -e "# thinks todo this week:"
    if [ -f "$do_each_week" ]; then
      cat "$do_each_week"
    fi
    echo -e "\n"

    echo -e "# things to do:"
    echo -e "\n"

    echo -e "# maybe:"
    echo -e "\n"

    echo -e "# waiting for:"
    echo -e "\n"

    echo -e "# inbox:"
    echo -e "\n"
  } >> "$file"

}

function open_todo_at_current_day_when_using_vim() {
  local file="$1"

  if ((offset_in_days == 0)); then
    if [[ "$editor" == *vim* ]]; then
      line="$(grep -n "$(date '+%A')" "$file" | cut -d':' -f1)"
      if [[ -n "$line" ]]; then
        # open the file at the line of the current week day
        $editor "+$line" -c "norm 0zt" "$file"
        return
      fi
    fi
  fi

  # in case it's not editing todo of the current week, no vim like editor is used
  # or the current week day was not found in the file
  # just open the file
  $editor "$file"
}

function editTodo() {
  local todo_file="$1"
  if [ -f "$todo_file" ]; then
    open_todo_at_current_day_when_using_vim "$todo_file"
  else
    if [ "$offset_in_days" -lt "0" ]; then
      echo "todo does not exist"
    else
      if ask_yes_no_question "Todo does not exist. Do you want to create it?"; then
        init_todo "$todo_file"
        open_todo_at_current_day_when_using_vim "$todo_file"
      fi
    fi
  fi
}

function getCurrentTodo () {
  echo "$todo_dir/$(get_weekly_todo_name "$@")"
}

# ----------------------------------------------------------------------------------------
ensure_todo_dir_exists
((offset_in_days=0))
if [[ "${*: -1}" =~ ^[+-]?[0-9]+$ ]]; then
  ((offset_in_days=${*: -1}*7))
else

  while getopts ":p" flag; do
    case "$flag" in
      p)
        case "$OPTARG" in
          d) echo "$todo_dir" ;;
          D) echo "$do_each_day" ;;
          W) echo "$do_each_week" ;;
          *)
            shift
            todo_full_path="$(get_todo_corresponding_to_offset)"
            if [ -f "$todo_full_path" ]; then
              echo "$todo_full_path"
            else
              exit 1
            fi
            ;;
        esac
        exit
      ;;
      *) echo "invalid argument" >&2; exit 1;;
    esac
  done
fi

if [[ "$#" -gt 1 ]]; then
  echo "to many arguments: $*"
  exit 1
fi

# set_editor_if_editor_does_not_exist
editTodo "$(get_todo_corresponding_to_offset)"
