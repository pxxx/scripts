#!/bin/bash

# Description: set wallpaper to all monitors using hsetroot; your can save the pictures and option hsetroot should use

declare optionDir="$HOME/.local/share/hsetroot"
declare currentPicture="$optionDir/currentWallpaper"
declare optionFile="$optionDir/option"
# declare hrootOption="-cover"
declare default_opt="-cover"

echo_red () {
  echo "$1" | while read -r line; do echo -e "\e[01;31m$line\e[0m"; done
}

error () {
  echo_red "$1" >&2
}

clear_hsetroot () {
  trash "$currentPicture" || rm "$currentPicture"
  hsetroot
}

get_opts () {
  cat "$optionFile"
}

set_w () {
  local opt="$(get_opts)"
  if [ -z "$opt" ]; then
    opt="$default_opt"
  fi
  echo "opt: $opt"
  hsetroot "$opt" "$currentPicture"
}

set_opt () {
  local option
  case "$1" in
    -fi|-fill|-f|f|fi) option="-fill" ;;
    -fu|-full|fu) option="-full" ;;
    -ce|-center|ce) option="-center" ;;
    -co|-cover|-c|c|co) option="-cover" ;;
    -e|-extend|e) option="-extend" ;;
  esac
  if [ -z "$option" ]; then
    return 1
  else
    echo "$option" > "$optionFile"
    return 0
  fi
}

save_current_picture() {
  if [ -f "$1" ]; then
    cp -f "$1" "$currentPicture"
  else
    error "faild to set: $1 as wallpaper"
    exit
  fi
}

random_from_array () {
  local -a array=("$@")
  echo "${array[(RANDOM % "${#array[@]}")]}"
}

random_from_dir () {
  local -a pictures
  readarray -t pictures <<<"$(find "$1" -type f)"
  local chosen
  chosen="$1$(random_from_array "${pictures[@]}")"
  echo "pic: $chosen"
  save_current_picture "$chosen"
  set_w
}

function set_wallpaper() {
  if [ "$1" == "-c" ] && [ -z "$2" ]; then
    clear_hsetroot
  elif [ "$1" == "-r" ] || [ -z "$1" ]; then # reset
    set_w
  elif [ -d "$1" ]; then
    random_from_dir "$1"
  elif [ -d "$2" ]; then
    set_opt "$1"
    random_from_dir "$2"
  elif [ -z "$2" ]; then # set wallpaper with default option
    cp -f "$1" "$currentPicture"
    set_w
  else
    set_opt "$1"
    save_current_picture "$2"
    set_w
  fi
}

ensure_file_structure() {
  if [ ! -d "$optionDir" ]; then
    mkdir -p "$optionDir"
  fi

  if [ ! -f "$optionFile" ]; then
    touch "$optionFile"
  fi
}


ensure_file_structure
set_wallpaper "$@"
