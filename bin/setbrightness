#!/bin/bash

# Description: Change the brightness of all monitors at ones to the same value using xrandr.

declare max_value=5

function limitBribghtness() {
  if [[ $(echo "$1<0" | bc) == 1 ]]; then
    echo "0"
  elif [[ $(echo "$1>$max_value"|bc) == 1 ]]; then
    echo "$max_value"
  else
    echo "$1"
  fi
}

function set_brightness() {
  for e in $(xrandr --listmonitors | tail -n+2 | awk '{print $NF}' | xargs echo); do
    xrandr --output "$e" --brightness "$1"
  done
}

function get_brightness() {
  xrandr --current --verbose | grep "Brightness" | head -n 1 |  awk '{print $NF}'
}

function print_help() {
cat << END_OF_HELP
### HELP ###

Description: Change the brightness of all monitors at ones to the same value using xrandr.
options:"
--increase, -i, increase, inc, i, + [double]  increase brightness by [double]
--decrease, -d, decrease, dec, d, - [double]  decrease brightness by [double]
--set, -s, s, = [double]                      set brightness [double]
--help, -h, help, h                           show this help menu
--get, -g, get, g                             get brightness (it's actually just the brightness of the first monitor)

The default option is 'set'.
The maximum brightness level that you can set is $max_value. You can change them by changing the value of the variable: 'max_value' within the script.

examples:
setbrightness - 0.1         decrease the brightness about 0.1 (10%)
setbrightness + 0.4         increase the brightness about 0.4 (40%)
setbrightness = 1.4         set brightness to 1.4 (140%)
setbrightness 1             set brightness to 1 (100%)

END_OF_HELP
}

function validate_args_length () {
  local suspected_number_of_args="$1"
  local -a args=("${@: 2}")
  local actual_number_of_args="${#args[@]}"
  if ((suspected_number_of_args != actual_number_of_args)); then
    echo "invalid arguments: $*" >&2
    echo
    print_help
    exit 1
  fi

}

declare brightness=""
case "$1" in
  inc|+|increase|--increase|-inc|-i|i)
    validate_args_length 2 "$@"
    brightness="$(limitBribghtness "$(echo "$(get_brightness) + $2" | bc)")"
  ;;
  dec|-|decrease|--decrease|-d|d)
    validate_args_length 2 "$@"
    brightness="$(limitBribghtness "$(echo "$(get_brightness) - $2" | bc)")"
  ;;
  set|=|--set|-s|s)
    validate_args_length 2 "$@"
    brightness="$(limitBribghtness "$2")" || print_help
  ;;
  -h|h|--help|help|'')
    validate_args_length 1 "$@"
    print_help
  ;;
  --get|-g|g|is)
    validate_args_length 1 "$@"
      get_brightness
  ;;
  *)
    if [[ "$1" =~ ^[0-9]+$|^[0-9]+\.[0-9]+$ ]] && [[ $# -eq 1 ]]; then
      brightness="$(limitBribghtness "$1")" || print_help
    else
      echo "invalid arguments: $*" >&2
      echo
      print_help
      exit 1
    fi
  ;;
esac

if [ -n "$brightness" ]; then
  echo "previous brightness:  $(get_brightness)"
  echo "set brightness:       $brightness"
  set_brightness "$brightness"
fi
