#!/bin/bash

# Description: This script toggles the touchpad.

xinput --list-props 14 | grep -q 'Device Enabled.*1$' &&
# (xinput --disable 14 && echo 'touch pad disabled') || (xinput --enable 14 && echo 'touch pad enabled') # do not sent notifications
(xinput --disable 14 && echo 'touch pad disabled'&& notify-send 'touch pad disabled') || (xinput --enable 14 && echo 'touch pad enabled' && notify-send 'touch pad enabled') # sent notifications
